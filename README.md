Geografia
============

How to run
--------------

```
docker-compose up -d
docker-compose run web ./manage.py migrate
docker-compose run web ./manage.py basedata
docker-compose run web ./manage.py createsuperuser --username admin --email admin@example.com
docker-compose run web ./manage.py arasaac
```

then point your browser to http://localhost:8000/admin/

### Admin tasks

```
docker-compose run web ./manage.py --help
```
