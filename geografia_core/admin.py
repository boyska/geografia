from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Utente

import geografia_core.models

# Register your models here.
for model in [
        geografia_core.models.Language,
        geografia_core.models.Word,
        geografia_core.models.PictureFile,
        geografia_core.models.PictureToWord,
        geografia_core.models.Picture,
        ]:
    admin.site.register(model)

admin.site.register(geografia_core.models.Utente, UserAdmin)
