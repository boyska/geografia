import urllib.parse

import requests

from django.core.files import File
from django.db import transaction
from django.core.management.base import BaseCommand, CommandError
from geografia_core.models import Language, Word, Picture, PictureFile


class Command(BaseCommand):
    help = "Import data from arasaac"
    api_url = "https://api.arasaac.org/api"

    def fetch_keywords(self, language: str) -> dict:
        url = "{}/keywords/{}".format(self.api_url, language)
        r = requests.get(url)
        r.raise_for_status()
        return r.json()

    def import_keywords(self, data: dict, langcode: str):
        words = data["words"]
        language = Language.objects.get(langcode=langcode)
        for w in words:
            Word.objects.get_or_create(language=language, text=w)

    def do_pictures(self, langcode: str):
        language = Language.objects.get(langcode=langcode)
        for word in Word.objects.filter(language=language):
            if not word.text.strip():
                continue
            search_url = "{}/pictograms/{}/bestsearch/{}".format(
                self.api_url, langcode, urllib.parse.quote(word.text, safe="")
            )
            r = requests.get(search_url)
            if r.status_code in [404, 400]:
                continue
            r.raise_for_status()
            data: list = r.json()
            if not data:
                raise ValueError(
                    "Unexpected: data for {}({}) is empty".format(word.text, langcode)
                )
            pictogram_id = data[0]["_id"]
            pictogram_url = "{}/pictograms/{}".format(self.api_url, pictogram_id)
            r = requests.get(pictogram_url, params={"download": "false"})
            r.raise_for_status()

            pf = PictureFile.objects.create(
                full_res=File(r.content), full_res_format="image/png"
            )
            pf.save()
            p = Picture.objects.create(parent=None, concrete=pf, selectable=True)
            p.save()
            p.words.add(word)
            p.save()

    def handle(self, *args, **options):
        languages = [l.langcode for l in Language.objects.all()]
        with transaction.atomic():
            for langcode in languages:
                try:
                    keywords = self.fetch_keywords(langcode)
                except Exception as exc:
                    self.stderr.write("Error: {}".format(exc))
                    continue
                self.import_keywords(keywords, langcode)

                self.do_pictures(langcode)
