from django.core.management.base import BaseCommand, CommandError
from geografia_core.models import Language

class Command(BaseCommand):
    help = 'Create base data for geografia'

    def handle(self, *args, **options):
        languages = [('it', 'Italiano', 'Pizza mamma mandolino')]
        for code, name, desc in languages:
            try:
                Language.objects.get(langcode=code)
            except Language.DoesNotExist:
                lang = Language.objects.create(langcode=code, fullname=name, description=desc)
                lang.save()
