from django.apps import AppConfig


class GeografiaCoreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'geografia_core'
    verbose_name = 'Dati Geografia'
