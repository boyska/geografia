import uuid

from django.db import models
from django.contrib.auth.models import AbstractUser

# dobbiamo fare un albero di immagini.
# ogni immagine viene associata a una o più parole, di cui una è la principale.


# Semantic model {{{


class Language(models.Model):
    langcode = models.CharField(max_length=3)  # XXX: validate that min_length=2)
    fullname = models.CharField(max_length=256)
    description = models.CharField(
        help_text="Additional explaination. ie: when to use this",
        max_length=256,
    )

    def __str__(self):
        return self.fullname


class Word(models.Model):
    """a word, really."""

    language = models.ForeignKey(Language, on_delete=models.PROTECT)
    text = models.CharField(max_length=256, db_index=True)

    class Meta:
        unique_together = [["language", "text"]]
        ordering = ["language", "text"]

    def __str__(self):
        return self.text


class PictureFile(models.Model):
    """
    specialized table that keeps references to images in many formats.
    """

    full_res = models.ImageField()
    full_res_format = models.CharField(max_length=64)
    thumbnail = models.ImageField()
    thumbnail_format = models.CharField(max_length=64)


class PictureToWord(models.Model):
    """specialized many-to-many, to allow adding extra fields in the future."""

    picture = models.ForeignKey("Picture", db_index=True, on_delete=models.CASCADE)
    word = models.ForeignKey(Word, db_index=True, on_delete=models.CASCADE)

    class Meta:
        unique_together = [["picture", "word"]]


class Picture(models.Model):
    # it's a tree! nodes link their parent.
    # There is a single parent "entity" from which everything descends
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    parent = models.ForeignKey(
        "self", blank=True, null=True, related_name="children", on_delete=models.PROTECT
    )

    concrete = models.ForeignKey(PictureFile, on_delete=models.PROTECT)
    words = models.ManyToManyField(Word, related_name="pictures", through=PictureToWord)
    selectable = models.BooleanField(db_index=True)

    def __str__(self):
        if not self.words.count():
            return 'Picture {}'.format(self.id)
        if self.words.count() == 1:
            return 'Picture<{}>'.format(self.words.all()[0].text)
        return 'Picture<{}+{}>'.format(self.words.all()[0].text, self.words.count()-1)


# Semantic model }}}


# Users, roles and stuff {{{


class Utente(AbstractUser):
    pass

    def __str__(self):
        return self.username


class Group:
    # XXX: check what's the proper way of subclassing user
    pass


# We're interested in having this groups:
#  - speakers . those are the people that use this webapp to improve their communication skills
#  - assistant . those people help speakers. They can add new words, check translations, etc.
#  - peers . those are people that speakers talk with. They can't do much, but can have a preview at what
#      their natural language would be translated to.

# In theory, if we think of this as a social network, it could be that Joe is only an assistant for Andrea,
# not to *any* speaker. Right now, we don't have this fine-grainedness at permission level.

# Users, roles and stuff }}}

# vim: set fdm=marker fdl=1:
